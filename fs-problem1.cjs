const path = require('path')
const fs = require('fs')

function problem1(directoryPath, numberOfFiles) {

    function createDirectory(directoryPath, numberOffiles, randomJSONFilesCb, deleteFilesCb) {
        fs.mkdir(directoryPath, function (error) {
            if (error) {
                randomJSONFilesCb(error)
            } else {
                randomJSONFilesCb(null, directoryPath, numberOffiles, deleteFilesCb)
            }
        })
    }
    createDirectory(directoryPath, numberOfFiles, randomJSONFiles, deleteFilesSimultaneously)
    function randomJSONFiles(error, directoryPath, numberOfFiles, deleteFilesCb) {
        let arrayOfFiles = []
        if (error) {
            console.log(error)
        }
        else {

            for (let index = 1; index <= numberOfFiles; index++) {
                const filePath = path.join(directoryPath, `randomFile${index}.json`)

                fs.writeFile(filePath, '{}', function (error) {
                    if (error) {
                        console.log(error)
                    } else {
                        arrayOfFiles.push(`randomFile${index}.json`)
                        if (arrayOfFiles.length === numberOfFiles) {

                            deleteFilesSimultaneously(null, directoryPath, arrayOfFiles)
                        }
                    }
                })
            }
        }


    }
    function deleteFilesSimultaneously(error, directoryPath, arrayOfFiles) {
        if (error) {
            console.log(error);
        } else {
         
            for (let index = 0; index < arrayOfFiles.length; index++) {

                const filePath = path.join(directoryPath, arrayOfFiles[index])
                fs.unlink(filePath, function (error) {
                    if (error) {
                        console.log(error);
                    }

                })
            }


        }

    }
}
module.exports = problem1

