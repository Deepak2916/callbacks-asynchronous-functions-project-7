const path = require('path')
const fsProblem1 = require('../fs-problem1.cjs')

const absolutePathOfRandomDirectory=path.resolve('test/randomDirectory')

let randomNumberOfFiles = Math.floor(Math.random() * 10)


fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles)