
const fs = require('fs')
const path = require('path')
let lipsumFile = path.join(__dirname, './lipsum.txt')
function problem2() {
    fs.readFile(lipsumFile, 'utf8', function (error, data) {
        if (error) {
            console.log(error)
        }
        data = data.toUpperCase()
        createNewFile('./filenames.txt', 'newfile1.txt' + '\n')
        createNewFile('./newfile1.txt', data)
    })
    function readFiles(filePath, newfile) {
        fs.readFile(filePath, 'utf8', function (error, data) {
            data = data.split('\n').sort().join('\n')
            createNewFile(`./${newfile}`, data)
            if (newfile === 'newfile3.txt') {
                appendFiles(newfile)
            } else {
                appendFiles(newfile + '\n')
            }
        })
    }
    function createNewFile(filePath, data) {
        console.log(`${filePath.slice(2)} created`)
        fs.writeFile(filePath, data, function (error) {
            if (error) {
                console.log(error)
            }
            else {
                if (filePath === './newfile1.txt') {
                    readFiles('./newfile1.txt', 'newfile2.txt')
                }
                else if (filePath === './newfile2.txt') {
                    readFiles('./newfile2.txt', 'newfile3.txt')
                }
                else if (filePath === './newfile3.txt') {
                    deleteFiles()
                }
            }
        })
    }
    function appendFiles(fileName) {
        fs.appendFile('./filenames.txt', fileName, (error) => {
            if (error) {
                console.log(error)
            }
        })
    }
    function deleteFiles() {
        fs.readFile('./filenames.txt', 'utf8', function (error, data) {
            data = data.split('\n')
            data.map(file => {
                fs.unlink(`${file}`, function (error) {
                    if (error) {
                        console.log(error)
                    } else {
                        console.log(`${file} deleted`)
                    }
                    return true
                })
            })
        })
    }
}
module.exports = problem2